﻿using Mohak.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Mohak.Controllers
{
    public class TestController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage a()
        {
            PaymentGateway pg = new PaymentGateway();
            var response = new HttpResponseMessage();
            response.Content = new StringContent(pg.GenerateCheckSumHash());
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }
        [HttpPost]
        public HttpResponseMessage b([FromBody]dynamic d)
        {
            PaymentGateway pg = new PaymentGateway();
            var response = new HttpResponseMessage();
            response.Content = new StringContent(pg.VerifyCheckSum(d));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }
    }
}
