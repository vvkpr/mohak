﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using paytm;

namespace Mohak.Utils
{
    public class PaymentGateway
    {
        public string GenerateCheckSumHash()
        {
            /* initialize a TreeMap object */
            Dictionary<String, String> paytmParams = new Dictionary<String, String>();

            /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
            paytmParams.Add("MID", Constants.MID);

            /* Find your WEBSITE in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
            paytmParams.Add("WEBSITE", Constants.WEBSITE);

            /* Find your INDUSTRY_TYPE_ID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
            paytmParams.Add("INDUSTRY_TYPE_ID", Constants.INDUSTRY_TYPE_ID);

            /* WEB for website and WAP for Mobile-websites or App */
            paytmParams.Add("CHANNEL_ID", Constants.CHANNEL_ID_WEB);

            /* Enter your unique order id */
            paytmParams.Add("ORDER_ID", "O-0000000005");

            /* unique id that belongs to your customer */
            paytmParams.Add("CUST_ID", "C-0000000001");

            /* customer's mobile number */
            paytmParams.Add("MOBILE_NO", "8460117542");

            /* customer's email */
            paytmParams.Add("EMAIL", "vivek8993parmar@gmail.com");

            /**
            * Amount in INR that is payble by customer
            * this should be numeric with optionally having two decimal points
*/
            paytmParams.Add("TXN_AMOUNT", "1.00");

            /* on completion of transaction, we will send you the response on this URL */
            paytmParams.Add("CALLBACK_URL", "https://localhost:44380/PaymentCallbackHandler.aspx");

            /**
            * Generate checksum for parameters we have
            * You can get Checksum DLL from https://developer.paytm.com/docs/checksum/
            * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
*/
            String checksum = paytm.CheckSum.generateCheckSum(Constants.MERCHANT_KEY, paytmParams);

            /* for Staging */
            String url = "https://securegw-stage.paytm.in/order/process";

            /* for Production */
            // String url = "https://securegw.paytm.in/order/process";

            /* Prepare HTML Form and Submit to Paytm */
            String outputHtml = "";
            outputHtml += "<html>";
            outputHtml += "<head>";
            outputHtml += "<title>Merchant Checkout Page</title>";
            outputHtml += "</head>";
            outputHtml += "<body>";
            outputHtml += "<center><h1>Please do not refresh this page...</h1></center>";
            outputHtml += "<form method='post' action='" + url + "' name='paytm_form'>";
            foreach (string key in paytmParams.Keys)
            {
                outputHtml += "<input type='hidden' name='" + key + "' value='" + paytmParams[key] + "'>";
            }
            outputHtml += "<input type='hidden' name='CHECKSUMHASH' value='" + checksum + "'>";
            outputHtml += "</form>";
            outputHtml += "<script type='text/javascript'>";
            outputHtml += "document.paytm_form.submit();";
            outputHtml += "</script>";
            outputHtml += "</body>";
            outputHtml += "</html>";
            return outputHtml;
        }

        public bool VerifyCheckSum(dynamic d)
        {
            String paytmChecksum = "";

            /* Create a Dictionary from the parameters received in POST */
            Dictionary<String, String> paytmParams = new Dictionary<String, String>();
            //foreach (string key in Request.Form.Keys)
            //{
            //    if (key.Equals("CHECKSUMHASH"))
            //    {
            //        paytmChecksum = Request.Form[key];
            //    }
            //    else
            //    {
            //        paytmParams.Add(key.Trim(), Request.Form[key].Trim());
            //    }
            //}

            /**
            * Verify checksum
            * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
*/
            bool isValidChecksum = CheckSum.verifyCheckSum("YOUR_KEY_HERE", paytmParams, paytmChecksum);
            if (isValidChecksum)
            {
                //Response.Write("Checksum Matched");
                return true;
            }
            else
            {
                //Response.Write("Checksum Mismatched");
                return false;
            }
        }
    }
}